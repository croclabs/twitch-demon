/**
 * This file will automatically be loaded by webpack and run in the "renderer" context.
 * To learn more about the differences between the "main" and the "renderer" context in
 * Electron, visit:
 *
 * https://electronjs.org/docs/latest/tutorial/process-model
 *
 * By default, Node.js integration in this file is disabled. When enabling Node.js integration
 * in a renderer process, please be aware of potential security implications. You can read
 * more about security risks here:
 *
 * https://electronjs.org/docs/tutorial/security
 *
 * To enable Node.js integration in this file, open up `main.js` and enable the `nodeIntegration`
 * flag:
 *
 * ```
 *  // Create the browser window.
 *  mainWindow = new BrowserWindow({
 *    width: 800,
 *    height: 600,
 *    webPreferences: {
 *      nodeIntegration: true
 *    }
 *  });
 * ```
 */

import './twitch-brands.png'
import './html/newPage.html'

import './../node_modules/electron-tabs/dist/electron-tabs.js';
import './index.css';

import { TabGroup} from 'electron-tabs';

console.log('👋 This message is being logged by "renderer.js", included via webpack');

const tabGroup = document.querySelector("tab-group") as TabGroup;

// Setup the default tab which is created when the "New Tab" button is clicked
tabGroup.setDefaultTab({
    title: "New Page",
    src: "./pages/newPage.html",
    active: true
});

// Do stuff
const tab = tabGroup.addTab({
    closable: false,
    title: null,
    iconURL: 'assets/twitch-brands.png',
    src: "https://twitch.tv/",
    active: true,
});
const pos = tab.getPosition();
console.log("Tab position is " + pos);

// fix view height bc of custom header
tabGroup.shadowRoot.querySelector('.views').setAttribute('style', 'height: calc(100vh - 63px)');

tabGroup.shadowRoot.querySelector('.tab').querySelectorAll('.tab-title, .tab-badge, .tab-close').forEach(el => {
    el.remove();
});
